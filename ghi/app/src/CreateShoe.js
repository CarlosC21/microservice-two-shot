import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";

function CreateShoe() {
  let [inputs, setInputs] = useState({});
  let [bins, setBins] = useState([]);
  let navigate = useNavigate();

  function addShoeDB() {
    console.log(inputs);
    const requestOptions = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(inputs),
    };

    fetch("http://localhost:8080/api/shoes/", requestOptions).then(
      navigate("/shoes", { replace: true })
    );
  }

  async function fetchBins() {
    const url = "http://localhost:8100/api/bins/";

    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setInputs({ ...inputs, bin: data.bins[0]["href"] });
      setBins(data.bins);
    }
  }

  useEffect(() => {
    fetchBins();
    console.log(inputs);
  }, []);

  function handleInput(e) {
    e.preventDefault();
    const name = e.target.name;
    const val = e.target.value;
    setInputs({ ...inputs, [name]: val });
  }

  return (
    <>
      <form>
        <div class="form-group">
          <label for="manufacturer-input">Manufacturer</label>
          <input
            type="text"
            name="manufacturer"
            class="form-control"
            id="manufacturer-input"
            placeholder="Enter manufacturer"
            onChange={handleInput}
          />
        </div>
        <div class="form-group">
          <label for="model-input">Model</label>
          <input
            type="text"
            name="model"
            class="form-control"
            id="model-input"
            placeholder="Enter model"
            onChange={handleInput}
          />
        </div>
        <div class="form-group">
          <label for="color-input">Color</label>
          <input
            type="text"
            name="color"
            class="form-control"
            id="color-input"
            placeholder="Enter color"
            onChange={handleInput}
          />
        </div>
        <div class="form-group">
          <label for="picture-input">Picture URL</label>
          <input
            type="text"
            name="picture_url"
            class="form-control"
            id="picture-input"
            placeholder="Enter picture url"
            onChange={handleInput}
          />
        </div>
        <div class="form-group">
          <label for="bin-input">Bin</label>
          <select
            type="text"
            name="bin"
            class="form-control"
            id="bin-input"
            placeholder="Enter picture url"
            onChange={handleInput}
          >
            {bins.map((bin) => {
              return (
                <option key={bin.href} value={bin.href}>
                  {bin.closet_name}
                </option>
              );
            })}
          </select>
        </div>
        <button type="submit" class="btn btn-primary" onClick={addShoeDB}>
          Submit
        </button>
      </form>
    </>
  );
}

export default CreateShoe;
