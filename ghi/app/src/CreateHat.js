import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";

function CreateHat() {
  let [inputs, setInputs] = useState({});
  let [locations, setLocations] = useState([]);
  let navigate = useNavigate();

  function addHatDB() {
    console.log(inputs);
    const requestOptions = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(inputs),
    };

    fetch("http://localhost:8090/api/hats/", requestOptions).then(
      navigate("/hats", { replace: true })
    );
  }

  async function fetchLocations() {
    const url = "http://localhost:8100/api/locations/";

    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setInputs({ ...inputs, location: data.locations[0]["href"] });
      setLocations(data.locations);
    }
  }

  useEffect(() => {
    fetchLocations();
    console.log(inputs);
  }, []);

  function handleInput(e) {
    e.preventDefault();
    const name = e.target.name;
    const val = e.target.value;
    setInputs({ ...inputs, [name]: val });
  }

  return (
    <>
      <form>
        <div class="form-group">
          <label for="fabric-input">Fabric</label>
          <input
            type="text"
            name="fabric"
            class="form-control"
            id="fabric-input"
            placeholder="Enter fabric"
            onChange={handleInput}
          />
        </div>
        <div class="form-group">
          <label for="style-input">Style</label>
          <input
            type="text"
            name="style"
            class="form-control"
            id="style-input"
            placeholder="Enter style"
            onChange={handleInput}
          />
        </div>
        <div class="form-group">
          <label for="color-input">Color</label>
          <input
            type="text"
            name="color"
            class="form-control"
            id="color-input"
            placeholder="Enter color"
            onChange={handleInput}
          />
        </div>
        <div class="form-group">
          <label for="picture-input">Picture URL</label>
          <input
            type="text"
            name="picture_url"
            class="form-control"
            id="picture-input"
            placeholder="Enter picture url"
            onChange={handleInput}
          />
        </div>
        <div class="form-group">
          <label for="location-input">Location</label>
          <select
            type="text"
            name="location"
            class="form-control"
            id="location-input"
            placeholder="Enter picture url"
            onChange={handleInput}
          >
            {locations.map((location) => {
              return (
                <option key={location.href} value={location.href}>
                  {location.closet_name}
                </option>
              );
            })}
          </select>
        </div>
        <button type="submit" class="btn btn-primary" onClick={addHatDB}>
          Submit
        </button>
      </form>
    </>
  );
}

export default CreateHat;
