import { useState, useEffect } from "react";
import HatCard from "./HatCard";
import { useNavigate } from "react-router-dom";
// import ShoeCard from "./ShoeCard";

function HatList() {
  let [hats, setHats] = useState([]);
  let navigate = useNavigate();

  const getAllHats = async () => {
    const url = "http://localhost:8090/api/hats/";

    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      console.log(data);
      setHats(data.hats);
    }
  };

  const deleteHat = (id) => {
    let newState = [];
    for (let hat of hats) {
      if (hat.id === id) {
        continue;
      }
      newState.push(hat);
    }
    setHats(newState);
  };

  const createHat = () => {
    navigate("/hats/create", { replace: true });
  };

  useEffect(() => {
    getAllHats();
  }, []);
  return (
    <>
      <button onClick={createHat}>Create a new hat</button>
      {hats.map((hat) => {
        return (
          <HatCard
            key={hat.id}
            id={hat.id}
            fabric={hat.fabric}
            style={hat.style}
            color={hat.color}
            imageURL={hat.picture_url}
            deleteHat={deleteHat}
          />
        );
      })}
    </>
  );
}

export default HatList;
