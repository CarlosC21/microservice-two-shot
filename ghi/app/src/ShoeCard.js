// import { useState, useEffect } from "react";

function ShoeCard({ id, brand, model, color, imageURL, deleteShoe }) {

  function removeShoeDB() {
    console.log('running new version 2')
    fetch(`http://localhost:8080/api/shoes/${id}`, { method: 'DELETE' })
    .then(() => {
      deleteShoe(id);
      console.log('Deleted successfully');
    });
  }

  return (
    <div className="card" style={{
      width: "90%",
      display: "flex",
      flexDirection: "row",
      justifyContent: "space-evenly",
      margin: "1em",
      padding: "1em"
    }}>
      <div style={{width: "45%"}}>
        <h2>{brand.toUpperCase()}</h2>
        <p>Model: {model}</p>
        <p>Color: {color}</p>
      </div>
      <div style={{
        maxWidth: "30vw",
        flexDirection: "row",
        justifyContent: "space-evenly",
      }}>
        <img
          alt=""
          src={imageURL}
          style={{
            height: "100px",
            objectFit: "contain",
            margin: "1em"
          }}
        />
        <button className="btn btn-danger" type="button" onClick={removeShoeDB}>Delete Shoe</button>
      </div>
    </div>
    // <BrowserRouter>
    //   <Nav />
    //   <div className="container">
    //     <Routes>
    //       <Route path="/shoes" element={<MainPage />} />
    //     </Routes>
    //   </div>
    // </BrowserRouter>
  );
}

export default ShoeCard;
