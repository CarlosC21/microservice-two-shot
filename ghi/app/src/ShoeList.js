import { useState, useEffect } from "react";
import ShoeCard from "./ShoeCard";
import { useNavigate } from "react-router-dom";

function ShoeList() {
  let [shoes, setShoes] = useState([]);
  let navigate = useNavigate();
  //   console.log("hello?");
  const getAllShoes = async () => {
    const url = "http://localhost:8080/api/shoes/";

    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      console.log(data);
      setShoes(data.shoes);
    }
  };

  const deleteShoe = (id) => {
    let newState = [];
    for (let shoe of shoes) {
      if (shoe.id === id) {
        continue;
      }
      newState.push(shoe);
    }
    setShoes(newState);
  };

  const createShoe = () => {
    navigate("/shoes/create", { replace: true });
  };

  useEffect(() => {
    getAllShoes();
  }, []);
  return (
    <>
      <button onClick={createShoe}>Create a new shoe</button>
      {shoes.map((shoe) => {
        return (
          <ShoeCard
            key={shoe.id}
            id={shoe.id}
            brand={shoe.manufacturer}
            model={shoe.model}
            color={shoe.color}
            imageURL={shoe.picture_url}
            deleteShoe={deleteShoe}
          />
        );
      })}
    </>
  );
}

export default ShoeList;
