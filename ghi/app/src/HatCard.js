function HatCard({ id, fabric, style, color, imageURL, deleteHat }) {
  function removeHatDB() {
    fetch(`http://localhost:8090/api/hats/${id}`, { method: "DELETE" }).then(
      () => {
        deleteHat(id);
        console.log("Deleted Successfully");
      }
    );
  }

  return (
    <div
      className="card"
      style={{
        width: "90%",
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-evenly",
        margin: "1em",
        padding: "1em",
      }}
    >
      <div style={{ width: "45%" }}>
        <h2>{fabric.toUpperCase()}</h2>
        <p>Style: {style}</p>
        <p>Color: {color}</p>
      </div>
      <div
        style={{
          maxWidth: "30vw",
          flexDirection: "row",
          justifyContent: "space-evenly",
        }}
      >
        <img
          alt=""
          src={imageURL}
          style={{
            height: "100px",
            objectFit: "contain",
            margin: "1em",
          }}
        />
        <button className="btn btn-danger" type="button" onClick={removeHatDB}>
          Delete Hat
        </button>
      </div>
    </div>
    // <BrowserRouter>
    //   <Nav />
    //   <div className="container">
    //     <Routes>
    //       <Route path="/shoes" element={<MainPage />} />
    //     </Routes>
    //   </div>
    // </BrowserRouter>
  );
}

export default HatCard;
