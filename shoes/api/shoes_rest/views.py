from django.http import JsonResponse
from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from .models import Shoe, BinVO
from common.json import ModelEncoder
import json


# Create your views here.

class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "import_href"
    ]
    def get_extra_data(self, o):
        return {"bin": o.closet_name}

class ShoeEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model",
        "color",
        "picture_url",
        "id"
    ]
    encoders={"bin": BinVOEncoder()}





@require_http_methods(["GET", "POST"])
def get_all_shoes(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeEncoder,
            safe=False
        )

    elif request.method == "POST":
        content = json.loads(request.body)

        try:
            bin = BinVO.objects.get(import_href=content["bin"])
            # print(bin)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Bin does not exist"},
                status=400
            )

        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeEncoder,
            safe=False
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def get_one_shoe(request, pk):
    if request.method == "DELETE":
        try:
            shoe = Shoe.objects.get(id=pk)
            shoe.delete()
            return JsonResponse(
                shoe,
                encoder=ShoeEncoder,
                safe=False
            )
        except Shoe.DoesNotExist:
            return JsonResponse({"message": "Shoe does not exist"})

    elif request.method == "PUT":
        content = json.loads(request.body)
        Shoe.objects.filter(id=pk).update(**content)
        return JsonResponse(
            content,
            encoder=ShoeEncoder,
            safe=False
        )