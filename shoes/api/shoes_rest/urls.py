from django.urls import path

from .views import get_all_shoes, get_one_shoe

urlpatterns = [
    path(
        "shoes/",
        get_all_shoes,
        name="shoes_list",
    ),
    path('shoes/<int:pk>/', get_one_shoe, name="shoe_detail"),

]
