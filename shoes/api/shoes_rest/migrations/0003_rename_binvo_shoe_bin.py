# Generated by Django 4.0.3 on 2022-07-27 23:15

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shoes_rest', '0002_binvo_import_href'),
    ]

    operations = [
        migrations.RenameField(
            model_name='shoe',
            old_name='binVO',
            new_name='bin',
        ),
    ]
