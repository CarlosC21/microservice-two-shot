from django.urls import path

from .views import get_all_hats, get_one_hat


urlpatterns = [
    path("hats/", get_all_hats, name="hats_list"),
    path("hats/<int:pk>/", get_one_hat, name="hat_detail"),
]
