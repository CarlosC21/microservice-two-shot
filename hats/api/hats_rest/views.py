# from django.http import JsonResponse
# from django.shortcuts import render
# from common.json import ModelEncoder
# from hats_rest.models import Hat
# from .models import LocationVO, Hat
# from django.views.decorators.http import require_http_methods
# import json

# # Create your views here.


# class LocationVODetailEncoder(ModelEncoder):
#     model = LocationVO
#     properties = [
#         "import_href"
#     ]

#     def get_extra_data(self, o):
#         return {"location": o.closet_name}

# class HatDetailEncoder(ModelEncoder):
#     model = Hat
#     properties = [
#         "fabric",
#         "style",
#         "color",
#         "picture_url",
#         "id",
#         "location",
#     ]

#     encoders = {"location": LocationVODetailEncoder()}


# class HatListEncoder(ModelEncoder):
#     model = Hat
#     properties = [
#         "fabric",
#         "style",
#         "color",
#     ]


# @require_http_methods(["GET", "POST"])
# def api_list_hats(request):
#     if request.method == "GET":
#         hats = Hat.objects.all()
#         return JsonResponse(
#             {"hats": hats},
#             encoder=HatDetailEncoder,
#             safe=False,
#         )
#     elif request.method == "POST":
#         content = json.loads(request.body)
#         print("CONTENTTT", content)
#         try:
#             location = LocationVO.objects.get(import_href=content["location"])
#             content["location"] = location
#         except LocationVO.DoesNotExist:
#             return JsonResponse(
#                 {"message": "location does not exist"},
#                 status=400
#             )
#         # print("CONTENTTT", content)
#         hat = Hat.objects.create(**content)
#         return JsonResponse(
#             hat, 
#             encoder=HatDetailEncoder,
#             safe=False,
#         )


# @require_http_methods(["DELETE", "PUT"])
# def api_detail_hats(request, pk):
#     if request.method == "DELETE":
#         try:
#             hat = Hat.objects.get(id=pk)
#             hat.delete()
#             return JsonResponse(
#                 hat,
#                 encoder=HatDetailEncoder,
#                 safe=False
#             )
#         except Hat.DoesNotExist:
#             return JsonResponse({"message": "Hat does not exist"})
#     else:
#         content = json.loads(request.body)
#         Hat.objects.filter(id=pk).update(**content)
#         return JsonResponse(
#             content,
#             encoder=HatDetailEncoder,
#             safe=False
#         )


from django.http import JsonResponse
from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from .models import LocationVO, Hat
from common.json import ModelEncoder
import json


# Create your views here.

class LocationVOEnconder(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href"
    ]
    def get_extra_data(self, o):
        return {"location": o.closet_name}

class HatEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style",
        "color",
        "picture_url",
        "id"
    ]
    encoders={"location": LocationVOEnconder()}





@require_http_methods(["GET", "POST"])
def get_all_hats(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatEncoder,
            safe=False
        )

    elif request.method == "POST":
        content = json.loads(request.body)

        try:
            location = LocationVO.objects.get(import_href=content["location"])
            # print(location)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "location does not exist"},
                status=400
            )
        
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatEncoder,
            safe=False
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def get_one_hat(request, pk):
    if request.method == "DELETE":
        try:
            hat = Hat.objects.get(id=pk)
            hat.delete()
            return JsonResponse(
                hat,
                encoder=HatEncoder,
                safe=False
            )
        except Hat.DoesNotExist:
            return JsonResponse({"message": "hat does not exist"})
    
    elif request.method == "PUT":
        content = json.loads(request.body)
        Hat.objects.filter(id=pk).update(**content)
        return JsonResponse(
            content,
            encoder=HatEncoder,
            safe=False
        )